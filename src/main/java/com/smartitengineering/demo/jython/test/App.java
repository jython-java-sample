package com.smartitengineering.demo.jython.test;

import java.io.FileReader;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import org.python.core.Py;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args)
        throws Exception {
        PythonInterpreter interp = new PythonInterpreter();
        interp.execfile("./src/main/python/InterfaceTest.py");
        PyObject pythonObject;
        PyObject returnedObject;
        pythonObject = interp.eval("MyTestInterface()");
        returnedObject = pythonObject.invoke("isDivisible",
            interp.eval("int(4)"),
            interp.eval("int(2)"));
        System.out.println(Py.py2boolean(returnedObject));
        TestName name = new TestName("Jobaer Chowdhury");
        System.out.println(name.isRead());
        returnedObject = pythonObject.invoke("printName", Py.java2py(name));
        System.out.println(name.isRead());
        returnedObject = pythonObject.invoke("getName");
        TestName herName = (TestName) Py.tojava(returnedObject, TestName.class.getName());
        System.out.println(herName.getName());
        pythonObject = interp.eval("MyRunnable()");
        Runnable runnable = (Runnable) Py.tojava(pythonObject, Runnable.class.getName());
        Thread thread = new Thread (runnable);
        thread.start();
        /**
         * The JSR-223 way. Using Jython as a scripting only language.
         */
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("python");
        engine.eval(new FileReader("./src/main/python/InterfaceTest.py"));
        engine.eval("print \"Dividing 6 by 3, is it divisible: \", MyTestInterface().isDivisible(6,3)");
    }
}
