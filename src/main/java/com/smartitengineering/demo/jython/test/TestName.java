/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.demo.jython.test;

/**
 *
 * @author imyousuf
 */
public class TestName {
    private String name;
    private boolean read;

    public TestName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
}
