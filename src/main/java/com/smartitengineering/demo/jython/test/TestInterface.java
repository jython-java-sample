/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.demo.jython.test;

/**
 *
 * @author imyousuf
 */
public interface TestInterface {
    public boolean isDivisible(int a, int b);
    public void printName(TestName name);
    public TestName getName();
}
