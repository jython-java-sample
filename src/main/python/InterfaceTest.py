import java, time
from com.smartitengineering.demo.jython.test import TestInterface, TestName
from java.lang import Runnable

class MyTestInterface (TestInterface):
	def isDivisible (self, a, b):
		c = a % b
		if c == 0 :
			return bool("true")
		else :
			return bool("false")

	def printName(self, name):
		print name.getName()
		name.setRead(bool("true"))
	def getName(self):
		testName = TestName("Farhana Kabir")
		return testName

class MyRunnable (Runnable):
        def run(self):
            print "This is a Runnable Thread without compilation"
            print "Time: ", time.time()
